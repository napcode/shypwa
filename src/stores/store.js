import { defineStore } from 'pinia'

export const useStore = defineStore({
  id: 'main',
  state: () => ({
    production: import.meta.env.PROD,
    url: null,
    token: null,
    defaultDateRange: '30',
    services: [],
    isLoading: false,
    requestFailed: null,
    startDate: null,
    endDate: null,
    isMobile: null,
  }),
  getters: {
    noData() {
      return Object.keys(this.services).length === 0
    },
    apiUrl() {
      const baseUrl = `${this.url}/api/dashboard/`
      if (this.startDate !== null && this.endDate !== null) {
        const startDate = this.startDate.toJSON().split('T')[0]
        const endDate = this.endDate.toJSON().split('T')[0]
        return `${baseUrl}?startDate=${startDate}&endDate=${endDate}`
      }

      return baseUrl
    },
    servicesSorted() {
      return [...this.services].sort(
        (a, b) => b.stats.session_count - a.stats.session_count,
      )
    },
  },
  actions: {
    async getDataFromApi() {
      this.isLoading = true
      this.requestFailed = null
      try {
        let response = await fetch(this.apiUrl, {
          mode: 'cors',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Token ${this.token}`,
          },
        })
        if (response.status !== 200) {
          this.requestFailed = 'yes'
        } else {
          const apiData = await response.json()
          if ('services' in apiData) {
            this.services = apiData['services']
            this.requestFailed = 'no'
          } else {
            this.requestFailed = 'yes'
          }
        }
      } catch (e) {
        this.requestFailed = 'yes'
      }
      this.isLoading = false
    },
    saveToLocalStore() {
      localStorage.setItem('url', this.url)
      localStorage.setItem('token', this.token)
      localStorage.setItem('defaultDateRange', this.defaultDateRange)
    },
    loadFromLocalStore() {
      this.url = localStorage.getItem('url')
      this.token = localStorage.getItem('token')
      this.defaultDateRange =
        localStorage.getItem('defaultDateRange') || this.defaultDateRange
      this.initDates()
    },
    initDates() {
      this.startDate = new Date()
      this.endDate = new Date()
      const dateRange = parseInt(this.defaultDateRange)
      this.startDate.setDate(this.endDate.getDate() - dateRange)
    },
    updateIsMobile() {
      this.isMobile = window.innerWidth < 769
    },
  },
})
